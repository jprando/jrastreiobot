﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JRastreio.Entity
{
    public class TelegramChat : TelegramFrom
    {
        public string Type { get; set; }

        [JsonConstructor]
        public TelegramChat(int id, string first_name, string last_name, string username, string type) :
            base(id, first_name, last_name, username)
        { Type = type; }
    }
}
