﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JRastreio.Entity
{
    public class AndamentoRastreioEntity
    {
        public DateTime DataHora { get; set; }
        public String Local { get; set; }
        public String Situacao { get; set; }
        public String Detalhamento { get; set; }

        public override string ToString()
        {
            return String.Format("{0} {1} - {2} - {3}, {4}",
                DataHora.ToShortDateString(),
                DataHora.ToShortTimeString(),
                Local, Situacao, Detalhamento);
        }
    }
}
