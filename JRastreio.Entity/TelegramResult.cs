﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JRastreio.Entity
{
    public class TelegramResult
    {
        public int UpdateId { get; set; }
        public TelegramMessage Message { get; set; }

        [JsonConstructor]
        public TelegramResult(int update_id, TelegramMessage message)
        {
            UpdateId = update_id;
            Message = message;
        }
    }
}
