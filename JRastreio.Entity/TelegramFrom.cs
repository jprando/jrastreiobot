﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JRastreio.Entity
{
    public class TelegramFrom
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }

        [JsonConstructor]
        public TelegramFrom(int id, string first_name, string last_name, string username)
        {
            Id = id;
            FirstName = first_name;
            LastName = last_name;
            UserName = username;
        }
    }
}
