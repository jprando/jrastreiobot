﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JRastreio.Entity
{
    public class TelegramMessage
    {
        public int MessageId { get; set; }
        public TelegramFrom From { get; set; }
        public TelegramChat Chat { get; set; }
        public int Date { get; set; }
        public string Text { get; set; }

        [JsonConstructor]
        public TelegramMessage(int message_id, TelegramFrom from, TelegramChat chat, int date, string text)
        {
            MessageId = message_id;
            From = from;
            Chat = chat;
            Date = date;
            Text = text;
        }
    }
}
