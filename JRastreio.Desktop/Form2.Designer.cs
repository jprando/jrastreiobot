﻿namespace JRastreio.Desktop
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TokenInput = new System.Windows.Forms.TextBox();
            this.VerificarBotao = new System.Windows.Forms.Button();
            this.TextoInput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // TokenInput
            // 
            this.TokenInput.Location = new System.Drawing.Point(12, 12);
            this.TokenInput.Name = "TokenInput";
            this.TokenInput.Size = new System.Drawing.Size(298, 20);
            this.TokenInput.TabIndex = 0;
            this.TokenInput.Text = "207665234:AAFbecsEdVPsvmEp59fmrBwIsNGHIjqov_E";
            // 
            // VerificarBotao
            // 
            this.VerificarBotao.Location = new System.Drawing.Point(316, 10);
            this.VerificarBotao.Name = "VerificarBotao";
            this.VerificarBotao.Size = new System.Drawing.Size(75, 23);
            this.VerificarBotao.TabIndex = 1;
            this.VerificarBotao.Text = "Verificar";
            this.VerificarBotao.UseVisualStyleBackColor = true;
            this.VerificarBotao.Click += new System.EventHandler(this.VerificarBotao_Click);
            // 
            // TextoInput
            // 
            this.TextoInput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextoInput.Location = new System.Drawing.Point(12, 38);
            this.TextoInput.Multiline = true;
            this.TextoInput.Name = "TextoInput";
            this.TextoInput.Size = new System.Drawing.Size(422, 239);
            this.TextoInput.TabIndex = 2;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 289);
            this.Controls.Add(this.TextoInput);
            this.Controls.Add(this.VerificarBotao);
            this.Controls.Add(this.TokenInput);
            this.Name = "Form2";
            this.Text = "Telegram API";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TokenInput;
        private System.Windows.Forms.Button VerificarBotao;
        private System.Windows.Forms.TextBox TextoInput;
    }
}