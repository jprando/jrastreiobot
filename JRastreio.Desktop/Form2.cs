﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using RestSharp;
using JRastreio.Entity;
using JRastreio.Desktop.Properties;

namespace JRastreio.Desktop
{
    public partial class Form2 : Form
    {
        const string TELEGRAM_URL_BASE = "https://api.telegram.org/bot{0}/";
        RestClient client = null;

        private String TelegramUrl { get { return String.Format(TELEGRAM_URL_BASE, TokenInput.Text); } }

        public Form2()
        {
            InitializeComponent();
            client = new RestClient(TelegramUrl);
        }

        private void VerificarBotao_Click(object sender, EventArgs e)
        {
            this.VerificarMensagem();
        }

        private async void VerificarMensagem()
        {
            try
            {
                TextoInput.Text = "requisitando";

                RestRequest request = new RestRequest("getupdates");
                request.AddParameter("offset", Settings.Default.update_id);
                IRestResponse response = client.Execute(request);

                TextoInput.Text += "respondido, verificando";
                TextoInput.Text += "verificado, analisado";

                string responseContet = response.Content;

                if (responseContet.StartsWith("{\"ok\":true"))
                    TextoInput.Text += "retorno OK, continuar";

                else { throw new Exception("problema com a comunicacao da API do Telegram, parar"); }

                Newtonsoft.Json.Linq.JObject retornoApi = Newtonsoft.Json.Linq.JObject.Parse(responseContet);
                bool retorno2 = (bool)retornoApi["ok"];
                if (!retorno2)
                {
                    throw new Exception("erro2");
                }

                var results = retornoApi["result"];

                TextoInput.Text = "";

                foreach (Newtonsoft.Json.Linq.JToken item in results)
                {
                    TelegramResult msg = JsonConvert.DeserializeObject<TelegramResult>(item.ToString());

                    /*sendMessage*/
                    await this.EnviarMensagem(msg);

                    TextoInput.Text += "update_id " + item["update_id"] + Environment.NewLine;
                    TextoInput.Text += "message " + item["message"]["message_id"] + " - " + item["message"]["text"] + Environment.NewLine;
                    TextoInput.Text += Environment.NewLine;

                    Settings.Default.update_id = msg.UpdateId;
                }
                Settings.Default.update_id++;
                Settings.Default.Save();
                TextoInput.Text += "Fim";
                ///TextoInput.Text = responseContet;
            }
            catch (Exception ex)
            {
                TextoInput.Text = "ERRO\r\n" + ex.Message;

                MessageBox.Show(ex.Message);
            }
        }

        private async Task EnviarMensagem(TelegramResult msg)
        {
            /*
            string url = string.Concat(TelegramUrl);
            string jsonData = "{\"chat_id\"=" + item["message"]["chat"]["id"] + ",\"text\"=\"" + item["message"]["chat"]["username"] + ", quando o sistema estiver terminado de ser implementado, vamos lhe responder..." + "\"}";
            */
            var request = new RestRequest("sendMessage", Method.POST);
            request.AddParameter("chat_id", msg.Message.Chat.Id);/// item["message"]["chat"]["id"]);
            request.AddParameter("text", "um dia eu retorno!" + Environment.NewLine + msg.Message.Text);
            IRestResponse response = await client.ExecuteTaskAsync(request);

            if (!response.Content.StartsWith("{\"ok\":true,"))
            { throw new Exception("erro ao responder..."); }

            await EnviarMapa(msg);
        }

        private async Task EnviarMapa(TelegramResult msg)
        {
            var request = new RestRequest("sendLocation", Method.POST);
            request.AddParameter("chat_id", msg.Message.Chat.Id);/// item["message"]["chat"]["id"]);
            request.AddParameter("latitude", "-20.3600853");
            request.AddParameter("longitude", "-40.6622298");
            /// request.AddParameter("reply_to_message_id", msg.Message.MessageId);
            await client.ExecuteTaskAsync(request);
        }
    }
}
