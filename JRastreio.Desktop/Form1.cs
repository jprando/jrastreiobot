﻿using JRastreio.BLL;
using JRastreio.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JRastreio.Desktop
{
    public partial class Form1 : Form
    {
        private CorreioBLL _bll = new CorreioBLL();

        private IEnumerable<AndamentoRastreioEntity> listaAndamento = null;

        private string FCodigoRastreio { get { return this.CodigoRastreioInput.Text.Trim(); } }
        private Exception FErroEx { set { MsgLabel.Text = (value != null ? value.Message : "..."); } }

        public Form1() { InitializeComponent(); }


        private async void BuscarAndamentoRastreio()
        {
            MsgLabel.Text = "Carregando, aguarde...";
            dataGridView1.DataSource = null;
            try
            {
                listaAndamento = await _bll.BuscarAndamentoRastreio(FCodigoRastreio);
            }
            finally
            {
                FErroEx = _bll.Erro;
            }
            dataGridView1.DataSource = listaAndamento;
            dataGridView1.AutoResizeColumns();
        }

        private void CarregarBotao_Click(object sender, EventArgs e) { this.BuscarAndamentoRastreio(); }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
        }
    }
}
