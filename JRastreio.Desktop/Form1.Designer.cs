﻿namespace JRastreio.Desktop
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CodigoRastreioTitulo = new System.Windows.Forms.Label();
            this.CodigoRastreioInput = new System.Windows.Forms.TextBox();
            this.CarregarBotao = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.MsgLabel = new System.Windows.Forms.Label();
            this.DataHoraCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LocalColuna = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SituacaoColuna = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DetalhamentoCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // CodigoRastreioTitulo
            // 
            this.CodigoRastreioTitulo.AutoSize = true;
            this.CodigoRastreioTitulo.Location = new System.Drawing.Point(12, 20);
            this.CodigoRastreioTitulo.Name = "CodigoRastreioTitulo";
            this.CodigoRastreioTitulo.Size = new System.Drawing.Size(82, 13);
            this.CodigoRastreioTitulo.TabIndex = 0;
            this.CodigoRastreioTitulo.Text = "Codigo Rastreio";
            // 
            // CodigoRastreioInput
            // 
            this.CodigoRastreioInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CodigoRastreioInput.Location = new System.Drawing.Point(100, 12);
            this.CodigoRastreioInput.Name = "CodigoRastreioInput";
            this.CodigoRastreioInput.Size = new System.Drawing.Size(157, 29);
            this.CodigoRastreioInput.TabIndex = 1;
            this.CodigoRastreioInput.Text = "DU356086028BR";
            // 
            // CarregarBotao
            // 
            this.CarregarBotao.Location = new System.Drawing.Point(263, 12);
            this.CarregarBotao.Name = "CarregarBotao";
            this.CarregarBotao.Size = new System.Drawing.Size(75, 29);
            this.CarregarBotao.TabIndex = 2;
            this.CarregarBotao.Text = "Carregar";
            this.CarregarBotao.UseVisualStyleBackColor = true;
            this.CarregarBotao.Click += new System.EventHandler(this.CarregarBotao_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DataHoraCol,
            this.LocalColuna,
            this.SituacaoColuna,
            this.DetalhamentoCol});
            this.dataGridView1.Location = new System.Drawing.Point(12, 47);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(656, 202);
            this.dataGridView1.TabIndex = 3;
            // 
            // MsgLabel
            // 
            this.MsgLabel.AutoSize = true;
            this.MsgLabel.Location = new System.Drawing.Point(344, 20);
            this.MsgLabel.Name = "MsgLabel";
            this.MsgLabel.Size = new System.Drawing.Size(16, 13);
            this.MsgLabel.TabIndex = 4;
            this.MsgLabel.Text = "...";
            // 
            // DataHoraCol
            // 
            this.DataHoraCol.DataPropertyName = "DataHora";
            this.DataHoraCol.Frozen = true;
            this.DataHoraCol.HeaderText = "Data Hora";
            this.DataHoraCol.Name = "DataHoraCol";
            this.DataHoraCol.ReadOnly = true;
            // 
            // LocalColuna
            // 
            this.LocalColuna.DataPropertyName = "Local";
            this.LocalColuna.HeaderText = "Local";
            this.LocalColuna.Name = "LocalColuna";
            this.LocalColuna.ReadOnly = true;
            // 
            // SituacaoColuna
            // 
            this.SituacaoColuna.DataPropertyName = "Situacao";
            this.SituacaoColuna.HeaderText = "Situacao";
            this.SituacaoColuna.Name = "SituacaoColuna";
            this.SituacaoColuna.ReadOnly = true;
            // 
            // DetalhamentoCol
            // 
            this.DetalhamentoCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DetalhamentoCol.DataPropertyName = "Detalhamento";
            this.DetalhamentoCol.HeaderText = "Detalhamento";
            this.DetalhamentoCol.Name = "DetalhamentoCol";
            this.DetalhamentoCol.ReadOnly = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 261);
            this.Controls.Add(this.MsgLabel);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.CarregarBotao);
            this.Controls.Add(this.CodigoRastreioInput);
            this.Controls.Add(this.CodigoRastreioTitulo);
            this.Name = "Form1";
            this.Text = "JRastreioBot - JDi";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CodigoRastreioTitulo;
        private System.Windows.Forms.TextBox CodigoRastreioInput;
        private System.Windows.Forms.Button CarregarBotao;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label MsgLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataHoraCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn LocalColuna;
        private System.Windows.Forms.DataGridViewTextBoxColumn SituacaoColuna;
        private System.Windows.Forms.DataGridViewTextBoxColumn DetalhamentoCol;
    }
}

