﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

using HtmlAgilityPack;
using JRastreio.Entity;

namespace JRastreio.DAL
{
    public class CorreioDAL
    {
        public Exception Erro = null;

        public async Task<IEnumerable<AndamentoRastreioEntity>> BuscarAndamentoRastreio(string codigoRastreio)
        {
            Erro = null;
            IList<AndamentoRastreioEntity> lista = new List<AndamentoRastreioEntity>();

            HtmlDocument html = new HtmlDocument();

            try { await RealizarRequisicaoHttp(html, codigoRastreio); }
            catch (Exception ex) { Erro = ex; }

            HtmlNodeCollection andamentosHtml = RecuperarTabelaAndamento(html);

            AndamentoRastreioEntity andamento = null;

            foreach (HtmlNode andamentoTr in andamentosHtml)
            {
                if (andamentoTr.XPath == "/html[1]/body[1]/font[2]/table[1]/colgroup[1]/colgroup[1]/colgroup[1]/tr[1]")
                    continue;

                HtmlNode dataHoraTdHtml = andamentoTr.SelectSingleNode("td[1]");
                HtmlNode localTdHtml = andamentoTr.SelectSingleNode("td[2]");
                HtmlNode situacaoHtml = andamentoTr.SelectSingleNode("td[3]");

                if (andamentoTr.ChildNodes.Count == 3)
                {
                    andamento = new AndamentoRastreioEntity();
                    andamento.DataHora = DateTime.Parse(dataHoraTdHtml.InnerText);
                    andamento.Local = localTdHtml.InnerText;
                    andamento.Situacao = situacaoHtml.InnerText;
                    lista.Add(andamento);
                }
                else if (andamentoTr.ChildNodes.Count == 1)
                {
                    andamento.Detalhamento = dataHoraTdHtml.InnerText;
                }
            }
            return lista;
        }

        private static HtmlNodeCollection RecuperarTabelaAndamento(HtmlDocument html)
        {
            return html
                .DocumentNode
                .SelectNodes("/html[1]/body[1]/font[2]/table[1]/colgroup[1]/colgroup[1]/colgroup[1]/tr");
        }

        private static async Task RealizarRequisicaoHttp(HtmlDocument html, string codigoRastreio)
        {
            using (HttpClient http = new HttpClient())
            {
                StringContent formData = new StringContent("Z_START=1&P_TIPO=001&P_LINGUA=001&P_ITEMCODE=" + codigoRastreio + "&P_COD_UNI=" + codigoRastreio);

                HttpResponseMessage response = await http.PostAsync(@"http://websro.correios.com.br/sro_bin/txect01$.QueryList", formData);
                response.EnsureSuccessStatusCode();

                Stream responseStream = await response.Content.ReadAsStreamAsync();
                html.Load(responseStream);
            }
        }
    }
}
