﻿using JRastreio.DAL;
using JRastreio.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JRastreio.BLL
{
    public class CorreioBLL
    {
        CorreioDAL _dal = new CorreioDAL();

        public Exception Erro { get { return _dal.Erro; } }

        public async Task<IEnumerable<AndamentoRastreioEntity>> BuscarAndamentoRastreio(string codigoRastreio)
        {
            if(string.IsNullOrWhiteSpace(codigoRastreio))
            {
                throw new Exception("Informe o codigo de rastreio");
            }
            return await _dal.BuscarAndamentoRastreio(codigoRastreio);
        }
    }
}
